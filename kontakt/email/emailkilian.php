<html>
<head>
    <meta charset="utf-8">
    <title>Email wird gesendet</title>
    <!-- Bootstrap-CSS -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Stile für diese Vorlage -->
    <link href="../../templates/body-template.css" rel="stylesheet">

    <!-- Sticky Footer  -->
    <link href="../../templates/sticky-footer.css" rel="stylesheet">
</head>
<body>
<?php

$captcha;
if (isset($_POST['g-recaptcha-response'])) {
    $captcha = $_POST['g-recaptcha-response'];
}
if (!$captcha) {
    echo '<div class="jumbotron">
  				<h1>Warte</h1>
  				<p>Bitte den Captcha anklicken</p>
				</div>';
    exit;
}
$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lddh_USAAAAABkt7Ib2RyfvhoHLuePkNjbnynXC&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
if ($response . success == false) {

    echo '<div class="jumbotron">
  				<h1>Stop</h1>
  				<p>Du bist ein Bot</p>
				</div>';
    exit;

} else {

    $empf = "kilian.baehr@susoberesaar.de";
    $betreff = "SuS Obere Saar - " . $_POST["betreff"];
    $text = $_POST["text"];
    $von = "Diese Mail wurde von " . $_POST["vorname"] . " " . $_POST["nachname"] . " geschrieben. Seine Email Adresse ist " . $_POST["email"];
    $Mail = $_POST["email"];
    $header = 'From: kontaktformular@susoberesaar.de' . "\r\n" .
        'Reply-To: noreply@susoberesaar.de' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $header2 = 'From: kontaktformular@susoberesaar.de' . "\r\n" .
        'Reply-To:' . "$Mail" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $antwort = "Ihre Mail wurde versendet \n \n \n Original Text: \n \n \n ";


    mail("$empf", "$betreff", "$text" . "\n \n \n \n" . "$von", "$header2");
    mail("$Mail", "$betreff", "$antwort" . "$text", "$header");


    echo '	
		<div class="startet-template">
		<div class="col-md-8">
                <div class="jumbotron">
                        <H1 class="text-center">Anfrage wird ausgeführt</H1>
                        <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                        80%
                        </div>
                        </div>
                </div>
                </div>
                </div>';

    sleep(5);

    echo '<script type="text/javascript">
           window.location = "../anfrage.html"
      </script>';
}
exit;

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
